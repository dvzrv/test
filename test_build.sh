#!/usr/bin/env bash
#
# This is a dummy script to create a few files

set -euo pipefail

version="${1}"
output_dir="output/"
release_types=('test' 'foo' 'bar')
_extension="" _release_type="" _release_type_dir=""

for _release_type in "${release_types[@]}"; do
  _release_type_dir="${output_dir}/${_release_type}/${_release_type}-${version}"
  mkdir -pv -- "${_release_type_dir}"
  for _extension in {efi,gz,iso,lkrn,pxe,qcow2,xz,zst}; do
    printf "TEST FILE\n" > "${_release_type_dir}/file.${_extension}"
  done
done

{
  printf '# TYPE version_info info\n'
  printf 'version_info{name="foo",description="Version of foo",version="1.0.0"} 1\n'
  printf 'version_info{name="bar",description="Version of bar",version="1.1.0"} 1\n'
  printf '# TYPE artifact_bytes gauge\n'
  printf 'artifact_bytes{name="foo",description="Size of foo"} 500\n'
  printf 'artifact_bytes{name="bar",description="Size of bar"} 1000\n'
  printf '# TYPE data_count summary\n'
  printf 'data_count{name="foo",description="Amount of foo"} 100\n'
  printf 'data_count{name="bar",description="Amount of bar"} 200\n'
} > output/metrics.txt
